package com.allensdroid.openmart.model;

import com.google.firebase.firestore.IgnoreExtraProperties;

import java.sql.Timestamp;
import java.util.Date;

/**
 * Created by allen on 3/15/18.
 */

@IgnoreExtraProperties
public class Post {

    //private String uid;
    private String title;
    private String desc;
    private String category;
    private String price;
    private Date date;
    private String delivery_state;
    private String location;
    private String photo_url1;
    private String photo_url2;
    private String photo_url3;
    private boolean live;

    private String shop_name;
    private String shop_phone;
    //private String shop_address;
    //private String shop_email;
    private String shop_desc;

    public Post() {
    }

    //All required fields only
    public Post(String title, String price, String category,
                String delivery_state, String shop_name, String shop_phone) {
        //this.uid = uid;
        this.title = title;
        this.desc = desc;
        this.category = category;
        this.price = price;
        this.delivery_state = delivery_state;
        //this.photo_url1 = url;
        this.shop_name = shop_name;
        this.shop_phone = shop_phone;
        this.date = new Timestamp(System.currentTimeMillis());
    }

    public boolean isLive() {
        return live;
    }

    public void setLive(boolean live) {
        this.live = live;
    }

    public String getPhoto_url2() {
        return photo_url2;
    }

    public void setPhoto_url2(String photo_url2) {
        this.photo_url2 = photo_url2;
    }

    public String getPhoto_url3() {
        return photo_url3;
    }

    public void setPhoto_url3(String photo_url3) {
        this.photo_url3 = photo_url3;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getDesc() {
        return desc;
    }

    public void setDesc(String desc) {
        this.desc = desc;
    }

    public String getCategory() {
        return category;
    }

    public void setCategory(String category) {
        this.category = category;
    }

    public String getPrice() {
        return price;
    }

    public void setPrice(String price) {
        this.price = price;
    }

    public Date getDate() {
        return date;
    }

    public void setDate(Date date) {
        this.date = date;
    }

    public String getDelivery_state() {
        return delivery_state;
    }

    public void setDelivery_state(String delivery_state) {
        this.delivery_state = delivery_state;
    }

    public String getLocation() {
        return location;
    }

    public void setLocation(String location) {
        this.location = location;
    }

    public String getPhoto_url1() {
        return photo_url1;
    }

    public void setPhoto_url1(String photo_url1) {
        this.photo_url1 = photo_url1;
    }

    public String getShop_name() {
        return shop_name;
    }

    public void setShop_name(String shop_name) {
        this.shop_name = shop_name;
    }

    public String getShop_phone() {
        return shop_phone;
    }

    public void setShop_phone(String shop_phone) {
        this.shop_phone = shop_phone;
    }

    public String getShop_desc() {
        return shop_desc;
    }

    public void setShop_desc(String shop_desc) {
        this.shop_desc = shop_desc;
    }
}
