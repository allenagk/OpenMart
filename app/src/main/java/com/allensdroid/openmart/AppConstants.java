package com.allensdroid.openmart;

/**
 * Created by allen on 3/17/18.
 */

public class AppConstants {

    //Firebase Database References
    public static final String POSTS_COLLECTION_REF = "posts";
    public static final String MESSAGES_COLLECTION_REF = "messages";

    //Firebase Storage References
    public static final String POST_IMAGES_STORAGE_REF = "post_images";

    //Bundle keys
    public static final String POST_DOC_ID_BUNDLE_KEY = "post_key";

}
