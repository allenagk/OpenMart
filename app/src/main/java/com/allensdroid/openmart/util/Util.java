package com.allensdroid.openmart.util;

import android.net.Uri;

import java.util.UUID;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * Created by allen on 3/17/18.
 */

public class Util {

    /*
    * Receive a int type price value and generate string price tag*/
    public static String getLocalPrice(String price) {
        String stringPrice = "Rs. " + price;
        return stringPrice;
    }

    //Validate the email
    public static boolean isValidEmail(String email) {
        String EMAIL_PATTERN = "^[a-zA-Z0-9#_~!$&'()*+,;=:.\"(),:;<>@\\[\\]\\\\]+@[a-zA-Z0-9-]+(\\.[a-zA-Z0-9-]+)*$";
        Pattern pattern = Pattern.compile(EMAIL_PATTERN);
        Matcher matcher;

        matcher = pattern.matcher(email);
        return matcher.matches();
    }

    //Generate random String UUID
    public static String randomUUID() {
        String randomUUID = UUID.randomUUID().toString();
        return randomUUID;
    }

    public static String getImageNameFromUri(Uri uri) {
        String url = uri.toString();
        String fileName = url.substring(url.lastIndexOf('/') + 1);
        return fileName;
    }
}
