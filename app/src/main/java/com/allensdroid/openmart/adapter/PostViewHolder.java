package com.allensdroid.openmart.adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.allensdroid.openmart.R;
import com.squareup.picasso.Picasso;

/**
 * Created by allen on 3/15/18.
 */

public class PostViewHolder extends RecyclerView.ViewHolder {


    View mView;

    public PostViewHolder(View itemView) {
        super(itemView);

        mView = itemView;
    }

    public void setTitle(String title) {
        TextView post_title = mView.findViewById(R.id.tv_title);
        post_title.setText(title);
    }

    public void setDesc(String desc) {
        TextView post_desc = mView.findViewById(R.id.tv_desc);
        post_desc.setText(desc);
    }

    public void setPrice(String price) {
        TextView post_desc = mView.findViewById(R.id.tv_price);
        post_desc.setText(price);
    }

    public void setDelivery(String delivery) {
        TextView post_desc = mView.findViewById(R.id.tv_delivery_status);
        post_desc.setText(delivery);
    }

    public void setImageView(String url, Context context) {
        ImageView imagePost = mView.findViewById(R.id.img_product);
        Picasso.with(context)
                .load(url)//.resize(50, 50)
                //.centerCrop()
                //.networkPolicy(NetworkPolicy.OFFLINE)
                .error(R.drawable.ic_menu_info)
                .into(imagePost);
    }

}
