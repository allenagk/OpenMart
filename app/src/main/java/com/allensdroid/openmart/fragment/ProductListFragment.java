package com.allensdroid.openmart.fragment;


import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.DividerItemDecoration;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.allensdroid.openmart.R;
import com.allensdroid.openmart.activity.ProductActivity;
import com.allensdroid.openmart.adapter.PostViewHolder;
import com.allensdroid.openmart.model.Post;
import com.firebase.ui.firestore.FirestoreRecyclerAdapter;
import com.firebase.ui.firestore.FirestoreRecyclerOptions;
import com.google.firebase.firestore.FirebaseFirestore;
import com.google.firebase.firestore.FirebaseFirestoreException;
import com.google.firebase.firestore.Query;

import static com.allensdroid.openmart.AppConstants.POSTS_COLLECTION_REF;
import static com.allensdroid.openmart.AppConstants.POST_DOC_ID_BUNDLE_KEY;
import static com.allensdroid.openmart.util.Util.getLocalPrice;

/**
 * A simple {@link Fragment} subclass.
 */
public class ProductListFragment extends Fragment {

    private static final String TAG = "ProductListFragment";
    private FirebaseFirestore db;
    private FirestoreRecyclerAdapter mAdapter;
    private LinearLayoutManager linearLayoutManager;
    private RecyclerView mPostList;

    private String category;
    private Query mQuery;


    public ProductListFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_product_list, container, false);

        db = FirebaseFirestore.getInstance();

        //Get Argument that passed from activity in "data" key value
        if (getArguments() != null) {
            //setupQueryFromArg(getArguments().getString("category"));
            category = getArguments().getString("category");
            if (category.equalsIgnoreCase("All")) {
                mQuery = db.collection(POSTS_COLLECTION_REF).whereEqualTo("live", true);
            } else {
                mQuery = db.collection(POSTS_COLLECTION_REF).whereEqualTo("category", category).whereEqualTo("live", true);
            }
        }

        mPostList = view.findViewById(R.id.post_list);
        //mPostList.setHasFixedSize(true);

        linearLayoutManager = new LinearLayoutManager(getContext(), LinearLayoutManager.VERTICAL, false);
        mPostList.addItemDecoration(new DividerItemDecoration(mPostList.getContext(),
                LinearLayoutManager.VERTICAL));
        mPostList.setLayoutManager(linearLayoutManager);

        return view;
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);

        getPostList(mQuery);

    }

    private void getPostList(Query query) {

        FirestoreRecyclerOptions<Post> response = new FirestoreRecyclerOptions.Builder<Post>()
                .setQuery(query, Post.class)
                .build();

        mAdapter = new FirestoreRecyclerAdapter<Post, PostViewHolder>(response) {

            @Override
            protected void onBindViewHolder(PostViewHolder holder, int position, Post model) {
                holder.setTitle(model.getTitle());
                holder.setDesc(model.getDesc());
                holder.setPrice(getLocalPrice(model.getPrice()));
                holder.setImageView(model.getPhoto_url1(), getContext());
                if (model.getDelivery_state().equalsIgnoreCase("Available")) {
                    holder.setDelivery(getString(R.string.delivery_available));
                }

                final String docId = getSnapshots().getSnapshot(position).getId();
                Log.d("DOC ID: ", docId);
                Log.d("RESULT TITLE: ", model.getTitle());
                Log.d("RESULT DATE: ", model.getDate().toString());


                holder.itemView.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        Intent intent = new Intent(getContext(), ProductActivity.class);
                        intent.putExtra(POST_DOC_ID_BUNDLE_KEY, docId);
                        startActivity(intent);
                    }
                });
            }

            @Override
            public PostViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
                View view = LayoutInflater.from(parent.getContext())
                        .inflate(R.layout.product_list_item, parent, false);

                return new PostViewHolder(view);
            }

            @Override
            public void onError(FirebaseFirestoreException e) {
                Log.e(TAG, "error : " + e.getMessage());
            }

        };

        mAdapter.startListening();
        mAdapter.notifyDataSetChanged();
        mPostList.setAdapter(mAdapter);

    }

    @Override
    public void onStart() {
        super.onStart();

        //mAdapter.startListening();
    }

    @Override
    public void onStop() {
        super.onStop();
        //mAdapter.stopListening();
    }
}
