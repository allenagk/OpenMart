package com.allensdroid.openmart.activity;

import android.graphics.PorterDuff;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.ProgressBar;
import android.widget.ScrollView;
import android.widget.TextView;
import android.widget.Toast;

import com.allensdroid.openmart.R;
import com.allensdroid.openmart.model.Post;
import com.allensdroid.openmart.util.Util;
import com.daimajia.slider.library.SliderLayout;
import com.daimajia.slider.library.SliderTypes.BaseSliderView;
import com.daimajia.slider.library.SliderTypes.DefaultSliderView;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.firebase.firestore.DocumentReference;
import com.google.firebase.firestore.DocumentSnapshot;
import com.google.firebase.firestore.FirebaseFirestore;

import java.util.HashMap;

import static com.allensdroid.openmart.AppConstants.POSTS_COLLECTION_REF;
import static com.allensdroid.openmart.AppConstants.POST_DOC_ID_BUNDLE_KEY;

public class ProductActivity extends AppCompatActivity {

    private View parentLayout;
    private String postDocID;
    private static final String TAG = "ProductActivity";
    private FirebaseFirestore db;

    private TextView tvTitle;
    private TextView tvDeliveryState;
    private TextView tvPrice;
    private TextView tvDesc;
    private TextView tvCategory;
    private TextView tvShopName;
    private TextView tvShopPhone;
    private TextView tvShopAddress;
    private TextView tvShopDesc;

    private SliderLayout mDemoSlider;
    private ScrollView scrollView;
    private ProgressBar progressBar;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_product);

        db = FirebaseFirestore.getInstance();

        getSupportActionBar().setTitle(getString(R.string.title_pdct_details));

        //Initialize the ui by findViewById
        initUi();

        Bundle extras = getIntent().getExtras();
        if (extras != null) {
            postDocID = extras.getString(POST_DOC_ID_BUNDLE_KEY);
            Log.d("DOC ID: ", postDocID);
            getPost(postDocID);
        } else {
            finish();
        }
    }

    //Initialize the ui by findviewbyid
    private void initUi() {

        scrollView = findViewById(R.id.scrollView);
        progressBar = findViewById(R.id.progressBar);
        mDemoSlider = findViewById(R.id.slider);

        tvTitle = findViewById(R.id.tv_pd_title);
        tvPrice = findViewById(R.id.tv_pd_price);
        tvDeliveryState = findViewById(R.id.tv_pd_delivery_state);
        tvDesc = findViewById(R.id.tv_pd_desc);
        tvCategory = findViewById(R.id.tv_pd_category);
        tvShopName = findViewById(R.id.tv_pd_shop_name);
        tvShopPhone = findViewById(R.id.tv_pd_shop_phone);
        tvShopDesc = findViewById(R.id.tv_pd_shop_desc);
        tvShopAddress = findViewById(R.id.tv_pd_shop_address);
    }

    //Set page status load or loaded
    private void setLoading(boolean loading) {
        if (loading) {
            scrollView.setVisibility(View.GONE);
            progressBar.setVisibility(View.VISIBLE);
        } else {
            scrollView.setVisibility(View.VISIBLE);
            progressBar.setVisibility(View.GONE);
        }
    }

    //Set textviews
    private void setViews(Post post) throws Exception {

        //Set images in slider view
        setImageSlider(post);

        tvTitle.setText(post.getTitle());
        tvPrice.setText(Util.getLocalPrice(post.getPrice()));
        tvDesc.setText(post.getDesc());
        tvCategory.setText(post.getCategory());
        tvShopName.setText(post.getShop_name());
        tvShopPhone.setText(post.getShop_phone());
        //tvShopEmail.setText(post.getShop_email());
        tvShopAddress.setText(post.getLocation());
        tvShopDesc.setText(post.getShop_desc());

        if (post.getDelivery_state().equalsIgnoreCase("Available")) {
            tvDeliveryState.setText(getString(R.string.delivery_available));
        }
    }

    private void setImageSlider(Post pos) {

        HashMap<String, String> url_maps = new HashMap<String, String>();
        url_maps.put("1", pos.getPhoto_url1());

        if (pos.getPhoto_url2() != null) {
            url_maps.put("2", pos.getPhoto_url2());
        }
        if (pos.getPhoto_url3() != null) {
            url_maps.put("3", pos.getPhoto_url3());
        }

        for (String name : url_maps.keySet()) {
            DefaultSliderView defaultSliderView = new DefaultSliderView(ProductActivity.this);
            defaultSliderView
                    .image(url_maps.get(name))
                    .setScaleType(BaseSliderView.ScaleType.FitCenterCrop);
            //.setOnSliderClickListener(this);

            defaultSliderView.bundle(new Bundle());
            defaultSliderView.getBundle()
                    .putString("extra", name);
            mDemoSlider.addSlider(defaultSliderView);
        }
        mDemoSlider.setPresetTransformer(SliderLayout.Transformer.Default);
        mDemoSlider.setPresetIndicator(SliderLayout.PresetIndicators.Center_Bottom);
        //mDemoSlider.setCustomAnimation(new DescriptionAnimation());
    }

    //Send request to get post details, if success set the textViews, else show error
    private void getPost(String doc_id) {
        setLoading(true);
        DocumentReference docRef = db.collection(POSTS_COLLECTION_REF).document(doc_id);
        docRef.get().addOnSuccessListener(new OnSuccessListener<DocumentSnapshot>() {
            @Override
            public void onSuccess(DocumentSnapshot documentSnapshot) {
                setLoading(false);
                Post post = documentSnapshot.toObject(Post.class);
                Log.d(TAG, "Post Title" + " => " + post.getTitle());
                try {
                    setViews(post);
                } catch (Exception e) {
                    Toast.makeText(ProductActivity.this,
                            getString(R.string.toast_went_wrong), Toast.LENGTH_SHORT).show();
                    e.printStackTrace();
                }
            }
        });
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.product_menu, menu);
        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_favorite) {
            Log.d("Fav: ", "Clicked");
            findViewById(R.id.action_favorite).setVisibility(View.GONE);
            return true;
        }

        return super.onOptionsItemSelected(item);
    }
}
