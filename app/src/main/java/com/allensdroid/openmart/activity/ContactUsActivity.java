package com.allensdroid.openmart.activity;

import android.app.ProgressDialog;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.text.TextUtils;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.allensdroid.openmart.R;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.firebase.firestore.DocumentReference;
import com.google.firebase.firestore.FirebaseFirestore;

import java.util.HashMap;
import java.util.Map;

import static com.allensdroid.openmart.AppConstants.MESSAGES_COLLECTION_REF;

public class ContactUsActivity extends AppCompatActivity {

    private static final String TAG = "ContactUsActivity";
    private View parentLayout;
    private EditText etContactName;
    private EditText etContactEmail;
    private EditText etContactMessage;
    private Button btnSend;
    private ProgressDialog mProgress;
    FirebaseFirestore db;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_contact_us);

        db = FirebaseFirestore.getInstance();

        getSupportActionBar().setTitle(getString(R.string.send_us));

        init();

    }

    private void init() {
        parentLayout = findViewById(android.R.id.content);
        etContactName = findViewById(R.id.et_contact_name);
        etContactEmail = findViewById(R.id.et_contact_email);
        etContactMessage = findViewById(R.id.et_contact_message);
        mProgress = new ProgressDialog(this);
        mProgress.setMessage(getString(R.string.please_wait));
    }

    private void showMessage(String message) {
        Snackbar.make(parentLayout, message, Snackbar.LENGTH_SHORT)
                .setAction("Action", null).show();
    }

    private void writeMessage(String name, String email, String message) {
        mProgress.show();
        // Create a new message
        Map<String, Object> msg = new HashMap<>();
        msg.put("name", name);
        msg.put("email", email);
        msg.put("message", message);

        // Add a new document with a generated ID
        db.collection(MESSAGES_COLLECTION_REF)
                .add(msg)
                .addOnSuccessListener(new OnSuccessListener<DocumentReference>() {
                    @Override
                    public void onSuccess(DocumentReference documentReference) {
                        mProgress.hide();
                        Log.d(TAG, "DocumentSnapshot added with ID: " + documentReference.getId());
                        Toast.makeText(ContactUsActivity.this,
                                "Your Message Sent", Toast.LENGTH_LONG).show();
                        finish();
                    }
                })
                .addOnFailureListener(new OnFailureListener() {
                    @Override
                    public void onFailure(@NonNull Exception e) {
                        mProgress.hide();
                        Log.w(TAG, "Error adding document", e);
                        showMessage("Sending Failed, Try Again");
                    }
                });

    }

    public void Send(View view) {
        String name = etContactName.getText().toString().trim();
        String email = etContactEmail.getText().toString().trim();
        String message = etContactMessage.getText().toString().trim();

        if (!TextUtils.isEmpty(name) && !TextUtils.isEmpty(email) && !TextUtils.isEmpty(message)) {
            writeMessage(name, email, message);
        } else {
            showMessage("Fields were not filled right. Please check again?");
        }
    }
}
