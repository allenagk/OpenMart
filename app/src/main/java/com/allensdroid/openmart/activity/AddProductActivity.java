package com.allensdroid.openmart.activity;

import android.app.ProgressDialog;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.text.TextUtils;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.Spinner;
import android.widget.Toast;

import com.allensdroid.openmart.R;
import com.allensdroid.openmart.model.Post;
import com.allensdroid.openmart.util.Util;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.firebase.firestore.FirebaseFirestore;
import com.google.firebase.storage.FirebaseStorage;
import com.google.firebase.storage.StorageReference;
import com.google.firebase.storage.UploadTask;

import static com.allensdroid.openmart.AppConstants.POSTS_COLLECTION_REF;
import static com.allensdroid.openmart.AppConstants.POST_IMAGES_STORAGE_REF;
import static com.allensdroid.openmart.util.Util.getImageNameFromUri;

public class AddProductActivity extends AppCompatActivity implements View.OnClickListener {

    private View parentLayout;
    private EditText etTitle;
    private EditText etPrice;
    private EditText etDesc;
    private EditText etLocation;
    private EditText etShopName;
    private EditText etShopPhone;
    private EditText etShopDesc;
    private Spinner spinnerCategory;
    private Spinner spinner;
    private ImageButton imgBtnPhotoUrl1;
    private ImageButton imgBtnPhotoUrl2;
    private ImageButton imgBtnPhotoUrl3;
    private Button submit;
    private ProgressDialog mProgress;

    private Uri mImageUri1 = null;
    private Uri mImageUri2 = null;
    private Uri mImageUri3 = null;

    private static final int GALLERY_REQUEST_1 = 1;
    private static final int GALLERY_REQUEST_2 = 2;
    private static final int GALLERY_REQUEST_3 = 3;

    private static final String TAG = "AddProductActivity";

    private FirebaseStorage mStorage;
    private FirebaseFirestore mDatabase;
    private Post newPost;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_add_product);

        mStorage = FirebaseStorage.getInstance();
        mDatabase = FirebaseFirestore.getInstance();

        getSupportActionBar().setTitle(getString(R.string.title_add_product));

        init();

    }

    private void init() {

        parentLayout = findViewById(android.R.id.content);
        etTitle = findViewById(R.id.et_title);
        etPrice = findViewById(R.id.et_price);
        etDesc = findViewById(R.id.et_desc);
        etLocation = findViewById(R.id.et_location);
        etShopName = findViewById(R.id.et_shop_name);
        etShopPhone = findViewById(R.id.et_shop_phone);
        etShopDesc = findViewById(R.id.et_shop_desc);
        spinnerCategory = findViewById(R.id.spinner_category);
        spinner = findViewById(R.id.spinner_delivery);
        imgBtnPhotoUrl1 = findViewById(R.id.imgbtn_photo1);
        imgBtnPhotoUrl2 = findViewById(R.id.imgbtn_photo2);
        imgBtnPhotoUrl3 = findViewById(R.id.imgbtn_photo3);
        submit = findViewById(R.id.btn_submit);
        mProgress = new ProgressDialog(this);
        mProgress.setMessage(getString(R.string.please_wait));

        submit.setOnClickListener(this);
        imgBtnPhotoUrl1.setOnClickListener(this);
        imgBtnPhotoUrl2.setOnClickListener(this);
        imgBtnPhotoUrl3.setOnClickListener(this);
    }

    private void setupSubmit() {

        String title = etTitle.getText().toString().trim();
        String price = etPrice.getText().toString().trim();
        String desc = etDesc.getText().toString().trim();
        String location = etLocation.getText().toString().trim();
        String shopName = etShopName.getText().toString().trim();
        String shopPhone = etShopPhone.getText().toString().trim();
        String shopDesc = etShopDesc.getText().toString().trim();
        String productCategory = spinnerCategory.getSelectedItem().toString().trim();
        String deliveryState = spinner.getSelectedItem().toString().trim();

        if (!TextUtils.isEmpty(title) && !TextUtils.isEmpty(price) && !TextUtils.isEmpty(shopName)
                && !TextUtils.isEmpty(shopPhone) && !TextUtils.isEmpty(deliveryState)
                && !TextUtils.isEmpty(productCategory)) {

            if (mImageUri1 != null) {

                newPost = new Post(title, price, productCategory, deliveryState, shopName, shopPhone);
                newPost.setDesc(desc);
                newPost.setLocation(location);
                newPost.setShop_desc(shopDesc);
                newPost.setLive(false);

                uploadPhoto(mImageUri1);

            } else {
                Snackbar.make(parentLayout, "You must upload at least 1 photo", Snackbar.LENGTH_LONG)
                        .setAction("Action", null).show();
            }

        } else {
            Snackbar.make(parentLayout, "Please fill all the required fields", Snackbar.LENGTH_LONG)
                    .setAction("Action", null).show();
        }

    }

    private void uploadPhoto(Uri image_uri) {
        mProgress.setMessage("Uploading " + getImageNameFromUri(image_uri));
        mProgress.show();

        StorageReference filePath = mStorage.getReference()
                .child(POST_IMAGES_STORAGE_REF).child(Util.randomUUID());

        filePath.putFile(image_uri).addOnSuccessListener(new OnSuccessListener<UploadTask.TaskSnapshot>() {
            @Override
            public void onSuccess(UploadTask.TaskSnapshot taskSnapshot) {
                mProgress.dismiss();
                Uri downloadUrl = taskSnapshot.getDownloadUrl();
                newPost.setPhoto_url1(downloadUrl.toString());

                if (mImageUri2 != null) {
                    newPost.setPhoto_url2(mImageUri2.toString());
                }
                if (mImageUri3 != null) {
                    newPost.setPhoto_url3(mImageUri3.toString());
                }
                //Adding details to database with photoUrl
                writeDocument(newPost);
                Toast.makeText(AddProductActivity.this, "Successfully submit for review", Toast.LENGTH_LONG).show();
                finish();

            }
        }).addOnFailureListener(new OnFailureListener() {
            @Override
            public void onFailure(@NonNull Exception e) {
                mProgress.dismiss();
                //Toast.makeText(AddPostActivity.this, "Something went wrong",
                //      Toast.LENGTH_SHORT).show();
                Snackbar.make(parentLayout, "Something went wrong", Snackbar.LENGTH_LONG)
                        .setAction("Action", null).show();
            }
        });
    }

    private void uploadAdditionalPhoto(Uri image_uri) {
        mProgress.setMessage("Please Wait.. " + getImageNameFromUri(image_uri));
        mProgress.show();

        StorageReference filePath = mStorage.getReference()
                .child(POST_IMAGES_STORAGE_REF).child(Util.randomUUID());

        if (image_uri == mImageUri2) {
            filePath.putFile(image_uri).addOnSuccessListener(new OnSuccessListener<UploadTask.TaskSnapshot>() {
                @Override
                public void onSuccess(UploadTask.TaskSnapshot taskSnapshot) {
                    mProgress.dismiss();
                    Uri downloadUrl = taskSnapshot.getDownloadUrl();
                    mImageUri2 = downloadUrl;

                }
            }).addOnFailureListener(new OnFailureListener() {
                @Override
                public void onFailure(@NonNull Exception e) {
                    mProgress.dismiss();
                    Snackbar.make(parentLayout, "Something went wrong", Snackbar.LENGTH_LONG)
                            .setAction("Action", null).show();
                }
            });
        } else if (image_uri == mImageUri3) {
            filePath.putFile(image_uri).addOnSuccessListener(new OnSuccessListener<UploadTask.TaskSnapshot>() {
                @Override
                public void onSuccess(UploadTask.TaskSnapshot taskSnapshot) {
                    mProgress.dismiss();
                    Uri downloadUrl = taskSnapshot.getDownloadUrl();
                    mImageUri3 = downloadUrl;

                }
            }).addOnFailureListener(new OnFailureListener() {
                @Override
                public void onFailure(@NonNull Exception e) {
                    mProgress.dismiss();
                    Snackbar.make(parentLayout, "Something went wrong", Snackbar.LENGTH_LONG)
                            .setAction("Action", null).show();
                }
            });
        }
    }

    private void writeDocument(Post post) {
        mDatabase.collection(POSTS_COLLECTION_REF).add(post);
    }

    @Override
    public void onClick(View view) {

        switch (view.getId()) {
            case R.id.btn_submit:
                setupSubmit();
                break;
            case R.id.imgbtn_photo1:
                Intent galleryIntent1 = new Intent(Intent.ACTION_GET_CONTENT);
                galleryIntent1.setType("image/*");
                startActivityForResult(galleryIntent1, GALLERY_REQUEST_1);
                break;
            case R.id.imgbtn_photo2:
                Intent galleryIntent2 = new Intent(Intent.ACTION_GET_CONTENT);
                galleryIntent2.setType("image/*");
                startActivityForResult(galleryIntent2, GALLERY_REQUEST_2);
                break;
            case R.id.imgbtn_photo3:
                Intent galleryIntent3 = new Intent(Intent.ACTION_GET_CONTENT);
                galleryIntent3.setType("image/*");
                startActivityForResult(galleryIntent3, GALLERY_REQUEST_3);
                break;
        }

    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if (requestCode == GALLERY_REQUEST_1 && resultCode == RESULT_OK) {
            mImageUri1 = data.getData();
            imgBtnPhotoUrl1.setScaleType(ImageView.ScaleType.CENTER_INSIDE);
            imgBtnPhotoUrl1.setImageURI(mImageUri1);

        }
        if (requestCode == GALLERY_REQUEST_2 && resultCode == RESULT_OK) {
            mImageUri2 = data.getData();
            imgBtnPhotoUrl2.setScaleType(ImageView.ScaleType.CENTER_INSIDE);
            imgBtnPhotoUrl2.setImageURI(mImageUri2);
            uploadAdditionalPhoto(mImageUri2);
        }
        if (requestCode == GALLERY_REQUEST_3 && resultCode == RESULT_OK) {
            mImageUri3 = data.getData();
            imgBtnPhotoUrl3.setScaleType(ImageView.ScaleType.CENTER_INSIDE);
            imgBtnPhotoUrl3.setImageURI(mImageUri3);
            uploadAdditionalPhoto(mImageUri3);
        }
    }
}
