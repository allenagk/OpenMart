package com.allensdroid.openmart.activity;

import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.NavigationView;
import android.support.design.widget.TabLayout;
import android.support.v4.view.GravityCompat;
import android.support.v4.view.ViewPager;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.allensdroid.openmart.BuildConfig;
import com.allensdroid.openmart.R;
import com.allensdroid.openmart.adapter.PostViewHolder;
import com.allensdroid.openmart.adapter.ViewPagerAdapter;
import com.allensdroid.openmart.fragment.ProductListFragment;
import com.allensdroid.openmart.model.Post;
import com.firebase.ui.firestore.FirestoreRecyclerAdapter;
import com.firebase.ui.firestore.FirestoreRecyclerOptions;
import com.google.firebase.firestore.FirebaseFirestore;
import com.google.firebase.firestore.FirebaseFirestoreException;
import com.google.firebase.firestore.Query;

import static com.allensdroid.openmart.AppConstants.POSTS_COLLECTION_REF;
import static com.allensdroid.openmart.AppConstants.POST_DOC_ID_BUNDLE_KEY;
import static com.allensdroid.openmart.util.Util.getLocalPrice;

public class MainActivity extends AppCompatActivity
        implements NavigationView.OnNavigationItemSelectedListener {

    private static final String TAG = "MainActivity";
    private FirebaseFirestore db;
    private FirestoreRecyclerAdapter mAdapter;
    private LinearLayoutManager linearLayoutManager;
    private RecyclerView mPostList;

    private TabLayout tabLayout;
    private ViewPager viewPager;
    private TextView tvAppVersion;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        viewPager = (ViewPager) findViewById(R.id.viewpagerMain);
        setupViewPager(viewPager);

        tabLayout = (TabLayout) findViewById(R.id.tabs);
        tabLayout.setupWithViewPager(viewPager);

        FloatingActionButton fab = (FloatingActionButton) findViewById(R.id.fab);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(MainActivity.this, AddProductActivity.class);
                startActivity(intent);
            }
        });

        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(
                this, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        drawer.addDrawerListener(toggle);
        toggle.syncState();

        NavigationView navigationView = (NavigationView) findViewById(R.id.nav_view);
        View header = navigationView.getHeaderView(0);
        tvAppVersion = header.findViewById(R.id.tv_app_version);
        setVersionName();
        navigationView.setNavigationItemSelectedListener(this);

        db = FirebaseFirestore.getInstance();

    }

    private void setupViewPager(ViewPager viewPager) {
        Bundle dataAllFragment = new Bundle();
        Bundle dataFashionFragment = new Bundle();
        Bundle dataTravelFragment = new Bundle();
        Bundle dataBodyBuildFragment = new Bundle();
        Bundle dataOtherFragment = new Bundle();

        ViewPagerAdapter adapter = new ViewPagerAdapter(getSupportFragmentManager());

        ProductListFragment allFragment = new ProductListFragment();
        dataAllFragment.putString("category", "All");
        allFragment.setArguments(dataAllFragment);

        ProductListFragment fashionFragment = new ProductListFragment();
        dataFashionFragment.putString("category", "Fashion");
        fashionFragment.setArguments(dataFashionFragment);

        ProductListFragment travelFragment = new ProductListFragment();
        dataTravelFragment.putString("category", "Travel");
        travelFragment.setArguments(dataTravelFragment);

        ProductListFragment bodyBuildingFragment = new ProductListFragment();
        dataBodyBuildFragment.putString("category", "Bodybuilding");
        bodyBuildingFragment.setArguments(dataBodyBuildFragment);

        ProductListFragment otherFragment = new ProductListFragment();
        dataOtherFragment.putString("category", "Other");
        otherFragment.setArguments(dataOtherFragment);

        adapter.addFragment(allFragment, "All");
        adapter.addFragment(fashionFragment, "Fashion");
        adapter.addFragment(travelFragment, "Travel");
        adapter.addFragment(bodyBuildingFragment, "Bodybuilding");
        adapter.addFragment(otherFragment, "Other");

        viewPager.setAdapter(adapter);
    }

    @Override
    public void onStart() {
        super.onStart();
    }

    @Override
    public void onStop() {
        super.onStop();
    }

    private void getPostList() {

        Query query = db.collection(POSTS_COLLECTION_REF).whereEqualTo("live", true);

        FirestoreRecyclerOptions<Post> response = new FirestoreRecyclerOptions.Builder<Post>()
                .setQuery(query, Post.class)
                .build();

        mAdapter = new FirestoreRecyclerAdapter<Post, PostViewHolder>(response) {

            @Override
            protected void onBindViewHolder(PostViewHolder holder, int position, Post model) {
                holder.setTitle(model.getTitle());
                holder.setDesc(model.getDesc());
                holder.setPrice(getLocalPrice(model.getPrice()));
                holder.setImageView(model.getPhoto_url1(), MainActivity.this);
                if (model.getDelivery_state().equalsIgnoreCase("Available")) {
                    holder.setDelivery(getString(R.string.delivery_available));
                }

                final String docId = getSnapshots().getSnapshot(position).getId();
                Log.d("DOC ID: ", docId);
                Log.d("RESULT TITLE: ", model.getTitle());
                Log.d("RESULT DATE: ", model.getDate().toString());


                holder.itemView.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        Intent intent = new Intent(MainActivity.this, ProductActivity.class);
                        intent.putExtra(POST_DOC_ID_BUNDLE_KEY, docId);
                        startActivity(intent);
                    }
                });
            }

            @Override
            public PostViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
                View view = LayoutInflater.from(parent.getContext())
                        .inflate(R.layout.product_list_item, parent, false);

                return new PostViewHolder(view);
            }

            @Override
            public void onError(FirebaseFirestoreException e) {
                Log.e(TAG, "error : " + e.getMessage());
            }

        };

        //mAdapter.startListening();
        mAdapter.notifyDataSetChanged();
        mPostList.setAdapter(mAdapter);

    }

    @Override
    public void onBackPressed() {
        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        if (drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawer(GravityCompat.START);
        } else {
            super.onBackPressed();
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    @SuppressWarnings("StatementWithEmptyBody")
    @Override
    public boolean onNavigationItemSelected(MenuItem item) {
        // Handle navigation view item clicks here.
        int id = item.getItemId();

        if (id == R.id.nav_home) {
            // Handle the camera action
        } else if (id == R.id.nav_about) {
            view_About();
        } else if (id == R.id.nav_share) {
            appShare();
        } else if (id == R.id.nav_send) {
            startActivity(new Intent(this, ContactUsActivity.class));
        }

        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        drawer.closeDrawer(GravityCompat.START);
        return true;
    }

    private void appShare() {
        try {
            Intent i = new Intent(Intent.ACTION_SEND);
            i.setType("text/plain");
            i.putExtra(Intent.EXTRA_SUBJECT, getResources().getString(R.string.app_name));
            String sAux = "\nA Place to Buy and Sell Just About Anything\n\n";
            sAux = sAux + "https://play.google.com/store/apps/details?id=com.allensdroid.openmart";
            i.putExtra(Intent.EXTRA_TEXT, sAux);
            startActivity(Intent.createChooser(i, getResources().getString(R.string.share_title)));
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void view_About() {
        AlertDialog.Builder alt_bld = new AlertDialog.Builder(MainActivity.this);
        LayoutInflater factory = LayoutInflater.from(MainActivity.this);
        final View textEntryview = factory.inflate(R.layout.about_dialog_layout, null);
        alt_bld.setView(textEntryview).setCancelable(true)
                .setNegativeButton("DISMISS"
                        , new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                dialog.cancel();
                            }
                        });
        AlertDialog alert = alt_bld.create();
        alert.setTitle(getResources().getString(R.string.about_title));
        //alert.setIcon(R.drawable.ic_launcher);
        alert.show();
        alert.getButton(AlertDialog.BUTTON_NEGATIVE).setTextColor(getResources().getColor(R.color.btn_color));
    }

    private void setVersionName() {
        String versionName = BuildConfig.VERSION_NAME;
        tvAppVersion.setText(getString(R.string.text_version) + " " + versionName);
    }
}
